call env.bat
@echo off

if not exist obj md obj
if not exist output md output
cd obj
if not exist image md image
cd ..

REM set PATH=e:\ToolChain\make381;e:\ToolChain\RISCVGCC810-win32\bin
REM set TOOLS_DIR=e:\ToolChain\RISCVGCC810

set NAME=LiteMApiTestApp
set VER=1.8
set TYPE=ALL

make -s %1 all

REM mkimage sign rsa.private 1 0xA7260102 ./output/%NAME%_%VER%.bin ./output/%NAME%_sign.bin

mkimage merge_bin 0xFF "./output/%NAME%_%VER%.bin 0x96000 ./../bin/SubApp_EMV_all.bin 0x00" ./output/%NAME%_M.bin

mkimage sign rsa.private 1 0xA7260102 ./output/%NAME%_M.bin ./output/%NAME%_sign.bin

set SRC_BIN_PATH=%cd%

copy /Y %SRC_BIN_PATH%\output\%NAME%_sign.bin %SRC_BIN_PATH%\..\..\release\
copy /Y %SRC_BIN_PATH%\output\%NAME%_sign.bin %SRC_BIN_PATH%\..\..\full_upgrade_package\

REM set FILE1=./../../release/LiteMApiTestApp_sign.bin
REM set FILE2=./../../release/SubApp_EMV_sign.bin

REM for %%A in (%FILE1%) do set fileSize=%%~zA
REM REM echo fileSize = %fileSize%
REM set /a fileSize+=512

REM set code=0123456789ABCDEF
REM set var=%fileSize%
REM set hexlen=0
REM :again
REM set /a tra=%var%%%16
REM call,set tra=%%code:~%tra%,1%%
REM set /a var/=16
REM set str=%tra%%str%
REM if %var% geq 16 goto again
REM set hexlen=%var%%str%
REM REM echo HEX = %hexlen%
REM REM mkimage merge_bin 0xFF "%FILE1% 0x94000 %FILE2% 0x00" ./../../release/ota.asc
REM mkimage merge_bin 0xFF "%FILE1% %hexlen% %FILE2% 0x00" ./../../release/ota.asc

pause

